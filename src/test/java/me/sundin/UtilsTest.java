package me.sundin;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;
import static me.sundin.Utils.findCommonChar;
import static org.junit.jupiter.api.Assertions.assertAll;

class UtilsTest {

    @Test
    void findCommonCharTest() {
        assertAll(
                  () -> assertThat(findCommonChar("vJrwpWtwJgWr", "hcsFMMfFFhFp")).isEqualTo('p'),
                  () -> assertThat(findCommonChar("jqHRNqRjqzjGDLGL", "rsFMfFZSrLrFZsSL")).isEqualTo('L'),
                  () -> assertThat(findCommonChar("PmmdzqPrV", "vPwwTWBwg")).isEqualTo('P'),
                  () -> assertThat(findCommonChar("wMqvLMZHhHMvwLHj", "bvcjnnSBnvTQFn")).isEqualTo('v'),
                  () -> assertThat(findCommonChar("ttgJtRGJ", "QctTZtZT")).isEqualTo('t'),
                  () -> assertThat(findCommonChar("CrZsJsPPZsGz", "wwsLwLmpwMDw")).isEqualTo('s'));
    }

    @Test
    void findCommonChar2Test() {
        assertAll(
                  () -> assertThat(findCommonChar("vJrwpWtwJgWrhcsFMMfFFhFp",
                                                  "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
                                                  "PmmdzqPrVvPwwTWBwg")).isEqualTo('r'),
                  () -> assertThat(findCommonChar("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
                                                  "ttgJtRGJQctTZtZT",
                                                  "CrZsJsPPZsGzwwsLwLmpwMDw")).isEqualTo('Z'));
    }
}
