package me.sundin;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;


class Day1Test {

    @Test
    void day1_part1() {
        assertThat(Day1.solve1()).isEqualTo(74198);
    }

    @Test
    void day1_part2() {
        assertThat(Day1.solve2()).isEqualTo(209914);
    }
        
}
