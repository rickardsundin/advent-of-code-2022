package me.sundin.day3;

import org.junit.jupiter.api.Test;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;


class Day3Test {

    Stream<Rucksack> contents =
        Stream.of(
                  "vJrwpWtwJgWrhcsFMMfFFhFp",
                  "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
                  "PmmdzqPrVvPwwTWBwg",
                  "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
                  "ttgJtRGJQctTZtZT",
                  "CrZsJsPPZsGzwwsLwLmpwMDw")
        .map(Rucksack::new);

    @Test
    void sharedItem() {
        assertThat(contents.map(Rucksack::sharedItem)).containsExactly('p', 'L', 'P', 'v', 't', 's');
    }

    @Test
    void priority() {
        assertThat(contents.map(Rucksack::priority)).containsExactly(16, 38, 42, 22, 20, 19);
    }

    @Test
    void day3_part1() {
        assertThat(Day3.solve1()).isEqualTo(7821);
    }

    @Test
    void day3_part2() {
         assertThat(Day3.solve2()).isEqualTo(2752);
    }
}
