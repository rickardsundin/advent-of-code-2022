package me.sundin.day2;

import org.junit.jupiter.api.Test;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;
import static me.sundin.day2.GameRound.Result.*;
import static me.sundin.day2.GameRound.Shape.*;


class Day2Test {

    Stream<GameRound> rounds = Stream.of("A Y", "B X", "C Z").map(GameRound::new);

    @Test
    void score() {
        assertThat(rounds.mapToInt(GameRound::score)).containsExactly(8, 1, 6);
    }

    @Test
    void result() {
        assertThat(rounds.map(GameRound::result)).containsExactly(WIN, LOSS, DRAW);
    }

    @Test
    void score2() {
        assertThat(rounds.map(GameRound::score2)).containsExactly(4, 1, 7);
    }

    @Test
    void desiredShape() {
        assertThat(rounds.map(GameRound::desiredShape)).containsExactly(ROCK, ROCK, ROCK);
    }

    @Test
    void day2_part1() {
        assertThat(Day2.solve1()).isEqualTo(17189);
    }

    @Test
    void day2_part2() {
        assertThat(Day2.solve2()).isEqualTo(13490);
    }
}
