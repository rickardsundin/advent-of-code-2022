package me.sundin;

import java.util.List;
import java.nio.file.Files;
import java.nio.file.Path;
import java.io.IOException;

public class Utils {

    public static List<String> linesFromFile(Path path) {
        try {
            return Files.readAllLines(path);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read file from " + path, e);
        }
    }

    public static char findCommonChar(String s1, String s2) {
        return (char) s1.chars()
            .filter(ch -> s2.indexOf(ch) >= 0)
            .findFirst()
            .getAsInt();
    }

    public static char findCommonChar(String s1, String s2, String s3) {
        return (char) s1.chars()
            .filter(ch -> s2.indexOf(ch) >= 0 && s3.indexOf(ch) >= 0 )
            .findFirst()
            .getAsInt();
    }
}
