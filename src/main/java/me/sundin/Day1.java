package me.sundin;

import java.nio.file.Path;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Day 1
 */
public class Day1
{
    /**
     * Return a sorted list of the number of calories for each elf, highest value first.
     */
    private static Stream<Long> calories() {
        final List<Long> calories = new ArrayList<>();
        List<String> lines = Utils.linesFromFile(Path.of("src/main/resources/day1/input.txt"));
        long currentCalories = 0;
        for (String line : lines) {
            if (line.isEmpty()) {
                calories.add(currentCalories);
                currentCalories = 0;
            } else {
                currentCalories += Long.valueOf(line);
            }
        }
        calories.add(currentCalories);
        return calories.stream().sorted(Comparator.reverseOrder());
    }

    /**
     * Return the highest number of calories for any elf.
     */
    public static long solve1() {
        return calories().findFirst().get();
    }

    /**
     * Return the sum of calories for the top three elves.
     */
    public static long solve2() {
        return calories()
            .limit(3)
            .mapToLong(Long::longValue)
            .sum();
    }
}
