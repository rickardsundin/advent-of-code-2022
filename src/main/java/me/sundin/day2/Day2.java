package me.sundin.day2;

import java.nio.file.Path;
import java.util.stream.Stream;

import me.sundin.Utils;

/**
 * Day 2
 */
public class Day2
{
    private static Stream<GameRound> gameRounds() {
        return Utils.linesFromFile(Path.of("src/main/resources/day2/input.txt"))
            .stream()
            .map(GameRound::new);
    }

    /**
     * Return the total score of all rock-paper-scissors rounds.
     */
    public static long solve1() {
        return gameRounds()
            .mapToLong(GameRound::score)
            .sum();
    }

    /**
     * Return the total score of all rock-paper-scissors rounds.
     */
    public static long solve2() {
        return gameRounds()
            .mapToLong(GameRound::score2)
            .sum();
    }
}
