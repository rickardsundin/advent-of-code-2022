package me.sundin.day2;

import static me.sundin.day2.GameRound.Result.*;
import static me.sundin.day2.GameRound.Shape.*;

class GameRound {

    private final Shape their;
    private final Shape my;
    private final Result desiredResult;

    GameRound(String round) {
        their = shape(round.charAt(0));
        my = shape(round.charAt(2));
        desiredResult = desiredResult(round.charAt(2));
    }

    Result result() {
        if (their == my) {
            return DRAW;
        }
        if (my == beats(their)) {
            return WIN;
        }
        return LOSS;
    }

    Result desiredResult(char res) {
        return switch (res) {
        case 'X' -> LOSS;
        case 'Y' -> DRAW;
        case 'Z' -> WIN;
        default -> throw new IllegalArgumentException("Illegal desired result: " + res);
        };
    }

    Shape shape(char move) {
        return switch (move) {
        case 'A', 'X' -> ROCK;
        case 'B', 'Y' -> PAPER;
        case 'C', 'Z' -> SCISSORS;
        default -> throw new IllegalArgumentException("Illegal move: " + move);
        };
    }

    Shape desiredShape() {
        return switch(desiredResult) {
        case WIN -> beats(their);
        case LOSS -> losesTo(their);
        case DRAW -> their;
        };
    }

    private int score(Shape shape) {
        return switch (shape) {
        case ROCK -> 1;
        case PAPER -> 2;
        case SCISSORS -> 3;
        };
    }

    private int score(Result result) {
        return switch (result) {
        case LOSS -> 0;
        case WIN -> 6;
        case DRAW -> 3;
        };
    }

    int score() {
        return score(my) + score(result());
    }

    int score2() {
        return score(desiredShape()) + score(desiredResult);
    }

    static enum Shape {
        ROCK, PAPER, SCISSORS
    }

    Shape beats(Shape s) {
        return switch (s) {
        case SCISSORS -> ROCK;
        case PAPER -> SCISSORS;
        case ROCK -> PAPER;
        };
    }
    
    Shape losesTo(Shape s) {
        return switch (s) {
        case ROCK -> SCISSORS;
        case SCISSORS -> PAPER;
        case PAPER -> ROCK;
        };
    }

    static enum Result {
        WIN, LOSS, DRAW
    }
}
