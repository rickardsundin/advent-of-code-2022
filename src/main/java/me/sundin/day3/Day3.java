package me.sundin.day3;

import java.nio.file.Path;
import java.util.Iterator;
import java.util.stream.Stream;

import me.sundin.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Day 3
 */
public class Day3
{
    private static Stream<String> inventories() {
        return Utils.linesFromFile(Path.of("src/main/resources/day3/input.txt")).stream();
    }

    /** Solve day 1 */
    public static int solve1() {
        return inventories()
            .map(Rucksack::new)
            .mapToInt(Rucksack::priority)
            .sum();
    }


    public static int solve2() {
        List<Group> groups = new ArrayList<>();
        Iterator<String> inventories = inventories().iterator();
        do {
            groups.add(new Group(inventories.next(),inventories.next(), inventories.next()));

        } while (inventories.hasNext());;
        return groups.stream().mapToInt(Group::priority).sum();
    }
}
