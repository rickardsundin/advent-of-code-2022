package me.sundin.day3;

import me.sundin.Utils;

public class Rucksack {

    public final String inventory;

    public Rucksack(String inventory) {
        this.inventory = inventory;
    }

    /**
     *  The list of items for each rucksack is given as characters all
     *  on a single line. A given rucksack always has the same number
     *  of items in each of its two compartments, so the first half of
     *  the characters represent items in the first compartment, while
     *  the second half of the characters represent items in the
     *  second compartment. */
    char sharedItem() {
        int mid = inventory.length()/2;
        return Utils.findCommonChar(inventory.substring(0, mid), inventory.substring(mid));
    }

    /**
     * Lowercase item types a(97) through z have priorities 1 through 26.
     * Uppercase item types A(65) through Z have priorities 27 through 52.
     */
    int priority() {
        char item = sharedItem();
        return (item >= 'a')
            ? item - 96
            : item - 38;
    }
}
