package me.sundin.day3;

import me.sundin.Utils;

public class Group {

    String inventory1;
    String inventory2;
    String inventory3;

    public Group(String inventory1, String inventory2, String inventory3) {
        this.inventory1 = inventory1;
        this.inventory2 = inventory2;
        this.inventory3 = inventory3;
    }

    char sharedItem() {
        return Utils.findCommonChar(inventory1, inventory2, inventory3);
    }

    int priority() {
        char item = sharedItem();
        return (item >= 'a')
            ? item - 96
            : item - 38;
    }
}
